package br.com.geoclient.helper;

/**
 * Classe para centralizar as constantes do APP
 * 
 * @author erinaldo.souza
 * @since 12-11-2016
 *
 */
public class ConstantsHelper {
	
	public static final String APP_NAME = "GeoClient";
	
	private ConstantsHelper() {
		//Essa classe n�o deve ser instanciada
	}

	
	/**
	 * Complementa o nome do app com o texto desejado
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @param text
	 * @return texto concatenando o nome do app + o parametro
	 */
	public static String extendsAppName(Object text) {
		return APP_NAME + text;
	}
}