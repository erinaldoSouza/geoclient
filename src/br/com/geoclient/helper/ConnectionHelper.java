package br.com.geoclient.helper;

import java.sql.SQLException;

import totalcross.db.sqlite.SQLiteUtil;
import totalcross.sql.Connection;
import totalcross.sql.ResultSet;
import totalcross.sql.Statement;
import totalcross.sys.Settings;

public class ConnectionHelper {
	private static SQLiteUtil sqLiteUtil;
	private static final String USER_ADMIN_LOGIN = "admin";
	private static final String USER_ADMIN_PASS = "admin";
	
	
	private ConnectionHelper() {
		//Essa classe n�o deve ser instanciada
	}
	
	/**
	 * Retorna a conex�o com o banco de dados.
	 * 
	 * pattern singleton, double-lock
	 * 
	 * @author erinaldo.souza 
	 * @since 12-11-2016
	 * @return objeto connection
	 * @throws SQLException 
	 */
	public static Connection getConnection() {
		try {
			
			if(sqLiteUtil == null) {
				synchronized (SQLiteUtil.class) {
					if(sqLiteUtil == null) {
						sqLiteUtil = new SQLiteUtil(Settings.appPath,"geoclient.db");
						buildDataBase();
					}
				}
			}
			
			//api implementa um singleton
			return sqLiteUtil.con();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Cria o banco de dados e insere o usu�rio default (admin, admin), ainda n�o exista, e abre uma conex�o com o banco. 
	 *
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 */
	private static void buildDataBase() {
		try {
		     
			Statement st = sqLiteUtil.con().createStatement();
		    st.execute("create table if not exists user (_id INTEGER PRIMARY KEY , name varchar, login varchar, password varchar)");
		    st.execute("create table if not exists client (_id INTEGER PRIMARY KEY, name varchar, country varchar, region integer)");

		    //Busca o usu�dio defaul - admin. E caso ele ainda n�oo exists, ser� criado.
		    ResultSet rs =  st.executeQuery("SELECT * FROM User u WHERE u.login = '" + USER_ADMIN_LOGIN + "' AND u.password = '" + USER_ADMIN_PASS +"'");
		    if(!rs.next()) {
			   st.execute("INSERT INTO user(name, login, password) VALUES('User Admin', 'admin', 'admin' )");
		    }
		    
		    st.close();
		    
		    LogHelper.writeLog(ConnectionHelper.class, "banco de dados configurado com sucesso, conex�o aberta.");
	    
		} catch (Exception e) {
	    	LogHelper.writeLog(ConnectionHelper.class, e.getMessage());
	    }
	}
}