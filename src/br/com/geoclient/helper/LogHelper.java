package br.com.geoclient.helper;

public class LogHelper {
	private LogHelper() {
		// Essa classe n�o deve ser instanciada
	}
	
	/**
	 * Escreve o log no aplicativo
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * 
	 * @param clazz
	 * @param log
	 */
	public static void writeLog(Class<?> clazz, String log) {
		System.out.println(clazz.getName() + ": " + log);
	}

}
