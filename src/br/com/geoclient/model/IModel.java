package br.com.geoclient.model;

/**
 * Interface comum entre os models do APP
 * 
 * @author erinaldo.souza
 * @since 12-11-2016
 *
 */
public interface IModel {
	public Long getId();	
}
