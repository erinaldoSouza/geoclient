package br.com.geoclient.model;

import br.com.geoclient.enums.RegionEnum;

public class Client implements IModel {

	private Long id;

	private String name;

	private String country;

	private RegionEnum region;

	public Client() {
	}
	
	public Client(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public RegionEnum getRegion() {
		return region;
	}

	public void setRegion(RegionEnum region) {
		this.region = region;
	}
	
	@Override
	public String toString() {
		return "Nome: " + name + ", Pa�s: " + country + ", regi�o: " + region;
	}
}