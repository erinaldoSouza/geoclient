package br.com.geoclient.main;

import br.com.geoclient.business.UserBusiness;
import br.com.geoclient.container.LoginContainer;
import br.com.geoclient.helper.ConnectionHelper;
import br.com.geoclient.helper.ConstantsHelper;
import br.com.geoclient.repository.UserDao;
import totalcross.sql.Connection;
import totalcross.ui.MainWindow;

/**
 * Classe main do APP
 * 
 * @author erinaldo.souza
 * @since 12-11-2016
 *
 */
public class GeoClient extends MainWindow {

	public GeoClient() {
		super(ConstantsHelper.APP_NAME, HORIZONTAL_GRADIENT);
	}

	@Override
	public void initUI() {
		Connection conn = ConnectionHelper.getConnection();
		new LoginContainer(new UserBusiness(new UserDao(conn))).show();
	}	
}