package br.com.geoclient.repository;

import java.sql.SQLException;

import br.com.geoclient.model.IModel;
import totalcross.sql.Connection;
import totalcross.sql.ResultSet;
import totalcross.sql.Statement;

public abstract class GenericDao<T> {
	
	protected Connection connection;
	
	public GenericDao(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Insere um objeto no banco de dados
	 * 
	 * @param model
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 */
	public abstract void insert(T model); 
	
	/**
	 * Recupera um determinado objeto no banco
	 * 
	 * PS: O campo id tem que estar preenchido
	 * 
	 * @param model 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @return objeto recuperado do banco de dados
	 */
	public ResultSet find(IModel model) {
		ResultSet rs = null;
		try {
			Statement st = connection.createStatement();
			rs = st.executeQuery("SELECT * FROM " + model.getClass().getSimpleName() + " WHERE _id = " + model.getId());
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rs;
	}
	
	/**
	 * Retorna uma lista de objetos de um determinado tipo do banco de dados
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @return lista de objetos do tipo passado por parāmetro
	 */
	public ResultSet findAll(IModel model) {
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM " + model.getClass().getSimpleName();
			Statement st = connection.createStatement();
			rs = st.executeQuery(sql);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	/**
	 * Deleta o objeto passado por parmāmetro do banco de dados
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @param model
	 */
	public void delete(IModel model) {
		try {
			Statement st = connection.createStatement();
			st.executeUpdate("DELETE FROM " + model.getClass().getSimpleName() + " WHERE _id = " + model.getId());
			st.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Atualiza um objeto no banco de dados
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @param model
	 */
	public abstract void update(T model);

}