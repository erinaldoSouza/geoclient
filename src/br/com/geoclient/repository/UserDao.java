package br.com.geoclient.repository;

import java.sql.SQLException;

import br.com.geoclient.model.User;
import totalcross.sql.Connection;
import totalcross.sql.ResultSet;
import totalcross.sql.Statement;

public class UserDao extends GenericDao<User> {
	
	public UserDao(Connection connection) {
		super(connection);
	}

	/**
	 * Recupera um objeto do tipo user, usando o login e senha como par�metros e busca
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2015
	 * @param params
	 * 
	 * @return objeto do tipo User encontrado, ou null, caso o objeto n�o seja encontrado no banco de dados
	 */
	public User findBy(String... params) {
		StringBuilder sql = new StringBuilder("SELECT * FROM User WHERE login = '?' AND password = '?'");
		User user = null;
		
		try {
			int paramIndex;
			for (String param : params) {
				paramIndex = sql.indexOf("?"); 
				sql.insert(paramIndex, param);
				paramIndex += param.length();
				sql.delete(paramIndex, paramIndex+1);
			}
			
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery(sql.toString());
			
			while(rs.next()) {
				user = new User();
				user.setId(rs.getLong("_id"));
				user.setLogin(rs.getString("login"));
				user.setName(rs.getString("name"));
				user.setPassword(rs.getLong("password"));
			}
			
			st.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}

	@Override
	public void insert(User model) {
		throw new RuntimeException("Metodo ainda n�o implementado");
	}

	@Override
	public void update(User model) {
		throw new RuntimeException("Metodo ainda n�o implementado");		
	}
}