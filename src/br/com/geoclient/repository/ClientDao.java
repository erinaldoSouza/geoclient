package br.com.geoclient.repository;

import br.com.geoclient.model.Client;
import totalcross.sql.Connection;
import totalcross.sql.Statement;

public class ClientDao extends GenericDao<Client> {

	public ClientDao(Connection connection) {
		super(connection);
	}

	/**
	 * Insere um cliente no banco de dados
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @param model
	 */
	@Override
	public void insert(Client model) {
		try {
			StringBuilder insert = new StringBuilder("INSERT INTO client(name, country, region)" + " VALUES("
					+ "'"+model.getName() + "','" + model.getCountry() + "'," + model.getRegion().ordinal() + ")");

			Statement st = connection.createStatement();
			st.executeUpdate(insert.toString());
			st.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Atualiza um cliente no banco de dados
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @param model
	 */
	@Override
	public void update(Client model) {
		try {
			StringBuilder insert = new StringBuilder("UPDATE client SET name = '" + model.getName() + "', country = '" + model.getCountry() 
					     + "', region = " + model.getRegion().ordinal() + " WHERE _id = " + model.getId());
					     
					

			Statement st = connection.createStatement();
			st.executeUpdate(insert.toString());
			st.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}