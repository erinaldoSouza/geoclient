package br.com.geoclient.container;

import br.com.geoclient.business.ClientBusiness;
import br.com.geoclient.helper.ConnectionHelper;
import br.com.geoclient.helper.ConstantsHelper;
import br.com.geoclient.model.User;
import br.com.geoclient.repository.ClientDao;
import totalcross.ui.Button;
import totalcross.ui.Label;
import totalcross.ui.MainWindow;
import totalcross.ui.event.ControlEvent;
import totalcross.ui.event.Event;
import totalcross.ui.gfx.Color;

public class HomeContainer extends BaseContainer {

	private User user;
	private ClientBusiness clientBusiness;
	private Button btNovo, btListar;

	public HomeContainer(User user) {
		this.user = user;
		MainWindow.getMainWindow().setTitle(ConstantsHelper.extendsAppName(" - " + this.user.getName()));
	}

	@Override
	public void initUI() {
		super.initUI();
		add(btNovo= new Button("Novo Cadastro"), CENTER-80, AFTER + 70, PARENTSIZE + 40, PREFERRED);
		add(btListar= new Button("Listar Cadastros"), CENTER+80, SAME, PARENTSIZE + 40, PREFERRED);

		btNovo.setBackColor(Color.WHITE);
		btListar.setBackColor(Color.WHITE);
		
		Label copyRight = new Label("SoftSite - Todos os direitos reservados - 2016");
		copyRight.setFont(font.adjustedBy(-7));
		add(copyRight, CENTER+50, BOTTOM, PARENTSIZE, PREFERRED);
	}

	@Override
	protected void init() {
		clientBusiness = new ClientBusiness(new ClientDao(ConnectionHelper.getConnection()));
	}
	
	@Override
	public void onEvent(Event event) {
		BaseContainer previsou = new HomeContainer(user);
		switch (event.type) {
		case ControlEvent.PRESSED:
			if(event.target.equals(btListar)) {
				new ClientListContainer(clientBusiness, previsou).show();

			} else {
				new ClientAddContainer(clientBusiness, previsou).show();;
			};
			break;
		}
		super.onEvent(event);
	}
}
