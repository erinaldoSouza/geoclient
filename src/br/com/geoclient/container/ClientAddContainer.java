package br.com.geoclient.container;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import br.com.geoclient.business.ClientBusiness;
import br.com.geoclient.enums.RegionEnum;
import br.com.geoclient.model.Client;
import totalcross.ui.Button;
import totalcross.ui.ComboBox;
import totalcross.ui.Edit;
import totalcross.ui.Label;
import totalcross.ui.Toast;
import totalcross.ui.event.ControlEvent;
import totalcross.ui.event.Event;
import totalcross.ui.gfx.Color;

public class ClientAddContainer extends BaseContainer {

	private ClientBusiness clientBusiness;
	private Edit edName;
	private ComboBox cbCountry;
	private ComboBox cbRegion;
	private Button btnSalvar;
	private String[] countries;
	private Button btnVoltar;
	private Client client;

	public ClientAddContainer(ClientBusiness clientBusiness, BaseContainer previous) {
		this.clientBusiness = clientBusiness;
		this.previousContainer = previous;
	}

	public ClientAddContainer(ClientBusiness clientBusiness, BaseContainer previous, Client client) {
		this(clientBusiness, previous);
		this.client = client;
	}

	@Override
	protected void init() {
	}

	@Override
	public void initUI() {
		super.initUI();
		configAvailableCountries();

		add(new Label("Cadastro de Clientes"), CENTER, AFTER + 50);

		add(new Label("Nome:"), LEFT, AFTER + 10);
		add(edName = new Edit(), SAME, SAME);
		edName.setRect(RIGHT, SAME, edName.getWidth() - 50, edName.getHeight());
		edName.setMaxLength(7);
		
		add(new Label("Pais: "), LEFT, AFTER + 10);
		add(cbCountry = new ComboBox(countries), LEFT, SAME, FILL, PREFERRED);
		cbCountry.setRect(RIGHT, SAME, cbCountry.getWidth() - 50, cbCountry.getHeight());

		add(new Label("Regi�o: "), LEFT, AFTER + 10);
		add(cbRegion = new ComboBox(RegionEnum.values()), LEFT, SAME, FILL, PREFERRED);
		cbRegion.setRect(RIGHT, SAME, cbRegion.getWidth() - 50, cbRegion.getHeight());

		if (client != null) {
			edName.setText(client.getName());
			cbCountry.setSelectedItem(client.getCountry());
			cbRegion.setSelectedItem(RegionEnum.values()[client.getRegion().ordinal()]);
		} else {
			cbCountry.setSelectedIndex(0);
			cbRegion.setSelectedIndex(0);
		}

		add(btnSalvar = new Button("Salvar"), RIGHT, AFTER + 50, PARENTSIZE + 40, PREFERRED);
		add(btnVoltar = new Button("Voltar"), LEFT, SAME, PARENTSIZE + 40, PREFERRED);
		btnSalvar.setBackColor(Color.WHITE);
		btnVoltar.setBackColor(Color.WHITE);
	}

	/**
	 * Monta um array com os nomes dos paises dispon�veis
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * 
	 * @return array com os nomes dos paises dispon�veis
	 */
	private void configAvailableCountries() {
		List<String> countriesList = new ArrayList<>();
		String country = null;

		for (Locale locale : Locale.getAvailableLocales()) {
			country = locale.getDisplayCountry();
			if (!country.trim().isEmpty()) {
				countriesList.add(locale.getDisplayCountry());
			}
		}

		Collections.sort(countriesList);
		countries = countriesList.toArray(new String[0]);
	}

	@Override
	public void onEvent(Event event) {
		switch (event.type) {
		case ControlEvent.PRESSED:
			if (event.target.equals(btnSalvar)) {
				if (client == null) {
					doInsert();
				} else {
					doUpdate();
				}
			} else if (event.target.equals(btnVoltar)) {
				getPreviouContainer().show();
			}
			break;
		}
	}

	private void doUpdate() {
		String message = null;
		if (!edName.getText().isEmpty()) {
			client.setName(edName.getText());
			client.setCountry(countries[cbCountry.getSelectedIndex()]);
			client.setRegion(RegionEnum.values()[cbRegion.getSelectedIndex()]);
			clientBusiness.update(client);
			message = "Cliente atualizado com sucesso";
		} else {
			message = "Todos os campos s�o obrigat�rios";
		}
		client = null;
		clear();
		Toast.show(message, 2000);


	}

	/**
	 * Monta o objeto Client para inserir no banco
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * 
	 */
	private void doInsert() {
		String message = null;
		if (!edName.getText().isEmpty()) {
			Client client = new Client();
			client.setName(edName.getText());
			client.setCountry(countries[cbCountry.getSelectedIndex()]);
			client.setRegion(RegionEnum.values()[cbRegion.getSelectedIndex()]);

			clientBusiness.insert(client);
			message = "Cliente cadastrado com sucesso";
		} else {
			message = "Todos os campos s�o obrigat�rios";
		}

		clear();
		Toast.show(message, 2000);
	}
}