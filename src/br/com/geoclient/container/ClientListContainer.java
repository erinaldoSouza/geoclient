package br.com.geoclient.container;

import java.util.List;

import br.com.geoclient.business.ClientBusiness;
import br.com.geoclient.model.Client;
import totalcross.ui.Button;
import totalcross.ui.Container;
import totalcross.ui.Grid;
import totalcross.ui.Label;
import totalcross.ui.TabbedContainer;
import totalcross.ui.event.ControlEvent;
import totalcross.ui.event.Event;
import totalcross.ui.gfx.Color;

public class ClientListContainer extends BaseContainer {

	private ClientBusiness clientBusiness;
	private TabbedContainer tp;

	public ClientListContainer(ClientBusiness clientBusiness, BaseContainer previous) {
		this.clientBusiness = clientBusiness;
		this.previousContainer = previous;
	}

	@Override
	protected void init() {
	}

	@Override
	public void initUI() {
		super.initUI();
		String tabCaptions[] = { "Clientes" };
		tp = new TabbedContainer(tabCaptions);
		tp.extraTabHeight = fmH / 2;
		add(tp, LEFT + fmH / 2, AFTER+10, FILL - fmH / 2, FILL);
		tp.setContainer(0, new ClientGrid());
	}

	private class ClientGrid extends Container {
		Grid grid;
		Button btnRemove, btnChange;
		private Button btnVoltar;

		public void initUI() {
			setBackColor(Color.WHITE);
			add(btnRemove = new Button("Remover"), LEFT, TOP + 2);
			add(btnChange = new Button("Editar"), CENTER, SAME);
			add(btnVoltar = new Button("Voltar"), RIGHT, SAME);
			
			btnRemove.setBackColor(Color.WHITE);
			btnVoltar.setBackColor(Color.WHITE);
			btnChange.setBackColor(Color.WHITE);
			add(new Label("Lista de Clientes"), CENTER, BOTTOM - 1);

			String[] gridCaptions = { "   ID   ", "     Nome     ", "    Pais    ", " Regi�o" };
			Grid.useHorizontalScrollBar = true;
			grid = new Grid(gridCaptions, false);
			Grid.useHorizontalScrollBar = false;
			grid.verticalLineStyle = Grid.VERT_NONE;
			add(grid, LEFT, AFTER + 2, FILL, FIT, btnRemove);

			Client client;
			List<Client> clients = clientBusiness.findAll();
			int size = clients.size();
			String items[][] = new String[size][4];
			
			for(int i =0; i<size; i++) {
					client = clients.get(i);
					items[i][0] = client.getId().toString();
					items[i][1] = client.getName();
					items[i][2] = client.getCountry();
					items[i][3] = client.getRegion().toString();
			}
			grid.setItems(items);
		}

		public void onEvent(Event event) {
			long id;
			
			switch (event.type) {
			case ControlEvent.PRESSED:
				if (event.target.equals(btnRemove)) {
					int idx = grid.getSelectedIndex();
					if (idx == -1) {
						return;
					}

					id = Long.valueOf(grid.getSelectedItem()[0]);
					
					clientBusiness.delete(new Client(id));
					
					grid.del(idx);
					repaint();
				
				} else if (event.target.equals(btnChange)) {
					int index = grid.getSelectedIndex();
					if (index == -1) {
						return;
					}
					
					id = Long.valueOf(grid.getSelectedItem()[0]);
					
					Client client = clientBusiness.find(new Client(id));
					new ClientAddContainer(clientBusiness, new ClientListContainer(clientBusiness, getPreviouContainer()), client).show();
					
					repaint();

				} else if(event.target.equals(btnVoltar)) {
					getPreviouContainer().show();
				}
				
				break;
			}
		}
	}
}