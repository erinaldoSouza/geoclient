package br.com.geoclient.container;

import totalcross.ui.Container;
import totalcross.ui.Control;
import totalcross.ui.ImageControl;
import totalcross.ui.MainWindow;
import totalcross.ui.image.Image;

public abstract class BaseContainer extends Container {
	protected ImageControl logoControl; 
	protected BaseContainer previousContainer;
	
	public BaseContainer() {
		init();
	}
	
	/**
	 * Configura a logo do app
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * 
	 * @return imagecontrol contendo a logo
	 */
	protected Control getLogo() {
		try {
			Image image = new Image("img/LogoSoftSite.png");
			image.setHwScaleFixedAspectRatio(100, true);
			logoControl = new ImageControl(image);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return logoControl;
	}
	/**
	 * Faz o swap para o conteiner chamador do metodo
	 * 
	 * @author erinaldo.souza
	 * @since12-11-2016
	 */
	public void show() {
	      MainWindow.getMainWindow().swap(this);
	}
	
	@Override
	public void initUI() {
		super.initUI();
		Control imageControl = getLogo();
		
		// No windows não tive problemas, no linux está retornando null
		if(imageControl != null) {
			add(getLogo(), CENTER, TOP);
		}		
	}
	
	protected abstract void init();

	public BaseContainer getPreviouContainer() {
		return previousContainer;
	}

	public void setPreviousContainer(BaseContainer previouContainer) {
		this.previousContainer = previouContainer;
	}
}
