package br.com.geoclient.container;

import br.com.geoclient.business.UserBusiness;
import br.com.geoclient.helper.ConnectionHelper;
import br.com.geoclient.model.User;
import br.com.geoclient.repository.UserDao;
import totalcross.sql.Connection;
import totalcross.ui.Button;
import totalcross.ui.Edit;
import totalcross.ui.Label;
import totalcross.ui.Toast;
import totalcross.ui.event.ControlEvent;
import totalcross.ui.event.Event;
import totalcross.ui.gfx.Color;

public class LoginContainer extends BaseContainer {

	private UserBusiness userBusiness;
	private Edit edLogin;
	private Edit edSenha;
	private Button btEntrar;

	public LoginContainer(UserBusiness userBusiness) {
		this.userBusiness = userBusiness;
	}

	@Override
	public void initUI() {
		super.initUI();
		add(new Label("Login: "), LEFT, AFTER + 50);
		add(edLogin = new Edit(), LEFT, AFTER);

		add(new Label("Senha: "), LEFT, AFTER + 10);
		add(edSenha = new Edit(), LEFT, AFTER);
		edSenha.setMode(Edit.PASSWORD);

		add(btEntrar = new Button("Entrar"), CENTER, AFTER + 50, PARENTSIZE + 40, PREFERRED);
		btEntrar.setBackColor(Color.WHITE);
	}

	@Override
	public void onEvent(Event event) {

		switch (event.type) {
		case ControlEvent.PRESSED:
			if (event.target.equals(btEntrar)) {
				// Caso o evento seja no bot�o Entrar, efetuar o procedimento de
				// login
				doLogin();
			}
			break;
		}
	}

	/**
	 * Realiza o login do usu�rio
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * 
	 */
	private void doLogin() {
		User user = null;
		String login = edLogin.getText(), senha = edSenha.getText();
		if (!login.isEmpty() && !senha.isEmpty()) {
			user = userBusiness.login(edLogin.getText(), edSenha.getText());
		}

		// Caso usu�ro n�o encontrato, exibir mensagem de valida��o
		if (user == null) {
			Toast.show("Login e/ou senha inv�lidos!", 2000);

			// Caso usu�rio v�lido, redireciona para o home
		} else {
			new HomeContainer(user).show();
		}
	}

	/**
	 * Inicia a conex�o com o banco e instancia os servi�os para User e Client
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * 
	 * @throws Exception
	 */
	@Override
	public void init() {
		Connection conn = ConnectionHelper.getConnection();
		userBusiness = new UserBusiness(new UserDao((conn)));
	}
}