package br.com.geoclient.business;

import java.util.List;

import br.com.geoclient.model.User;
import br.com.geoclient.repository.UserDao;

public class UserBusiness implements GenericBusiness<User> {

	private UserDao userDao;
	
	public UserBusiness(UserDao userDao) {
		this.userDao = userDao;
	}
	
	@Override
	public void insert(User model) {
		throw new RuntimeException("Metodo ainda n�o implementado!");
	}

	@Override
	public User find(User user) {
		throw new RuntimeException("Metodo ainda n�o implementado!");
	}

	@Override
	public List<User> findAll() {
		throw new RuntimeException("Metodo ainda n�o implementado!");
	}

	@Override
	public void delete(User model) {
		throw new RuntimeException("Metodo ainda n�o implementado!");
	}

	/**
	 * Recupera um objeto do tipo User, tendo como parametro de busca o login e senha
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * 
	 * @param login
	 * @param senha
	 * 
	 * @return o user encontrado, ou null, caso o user n�o eesteja no banco de dados
	 */
	public User login(String login, String senha) {
		User user = userDao.findBy(login, senha);
		return user;
	}

	@Override
	public void update(User model) {
		throw new RuntimeException("Metodo ainda n�o implementado!");
	}
}