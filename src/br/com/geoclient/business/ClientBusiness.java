package br.com.geoclient.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.geoclient.enums.RegionEnum;
import br.com.geoclient.helper.LogHelper;
import br.com.geoclient.model.Client;
import br.com.geoclient.repository.ClientDao;
import totalcross.sql.ResultSet;

public class ClientBusiness implements GenericBusiness<Client> {

	private ClientDao clientDao;

	public ClientBusiness(ClientDao clientDao) {
		this.clientDao = clientDao;
	}

	/**
	 * Verifica se os campos obrigatorios est�o preenchidos, caso sim, insere o
	 * cliente no banco de dados
	 * 
	 * @param client
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 */
	@Override
	public void insert(Client client) {
		clientDao.insert(client);
		LogHelper.writeLog(getClass(), "Cliente inserido com sucesso. Dados(" + client + ")");
	}

	/**
	 * Recupera um cliente no banco de dados
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * 
	 * @return cliente encontrado, ou null, caso o cliente n tenha cadastro
	 */
	@Override
	public Client find(Client client) {
		ResultSet rs = clientDao.find(client);
		Client clientFound = null;

		try {
			while (rs.next()) {
				clientFound = fillClient(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return clientFound;
	}

	/**
	 * Cria um objeto do tipo Client, com os dados no ResultSet passado por
	 * par�metro
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * 
	 * @param rs
	 * @return objeto cliente com os dados do resultset
	 */
	private Client fillClient(ResultSet rs) {
		Client clientFound = null;
		try {
			clientFound = new Client();
			clientFound.setId(rs.getLong("_id"));
			clientFound.setName(rs.getString("name"));
			clientFound.setCountry(rs.getString("country"));
			clientFound.setRegion(RegionEnum.values()[rs.getInt("region")]);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return clientFound;
	}

	/**
	 * Recupera todos os clientes cadastrados no banco de dados
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 */
	@Override
	public List<Client> findAll() {
		List<Client> clients = new ArrayList<>();
		try {
			ResultSet rs = clientDao.findAll(new Client());
			if (rs != null) {
				while (rs.next()) {
					clients.add(fillClient(rs));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return clients;
	}

	/**
	 * Delete um cliente do banco de dados
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 */
	@Override
	public void delete(Client client) {
		clientDao.delete(client);
		LogHelper.writeLog(getClass(), "Cliente de id: " + client.getId() + " deletado com sucesso");
	}

	/**
	 * Atualiza um cliente no banco de dados
	 * 
	 * @param model
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 */
	@Override
	public void update(Client client) {
		clientDao.update(client);
		LogHelper.writeLog(getClass(), "Cliente de id: " + client.getId() + " atualizado com sucesso. Dados(" + client + ")");
	}
}