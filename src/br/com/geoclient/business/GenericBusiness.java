package br.com.geoclient.business;

import java.util.List;

public interface GenericBusiness<T>  { 
	
	/**
	 * Executa as regras e, caso sej valido, insere um objeto no banco de dados
	 * 
	 * @param model
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 */
	public void insert(T model);
	
	/**
	 * Executa as regras e, caso sej valido, recupera um determinado objeto no banco
	 * 
	 * @param model 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @return objeto recuperado do banco de dados
	 */
	public T find(T model);
	
	/**
	 * Retorna uma lista de objetos de um determinado tipo
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @return lista de objetos do tipo passado por parāmetro
	 */
	public List<T> findAll();
	
	/**
	 * Executa as regras e, caso seja valido, deleta o objeto passado por parmāmetro do banco de dados
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @param model
	 */
	public void delete(T model);
	
	/**
	 * Atualiza um objeto no banco de dados
	 * 
	 * @author erinaldo.souza
	 * @since 12-11-2016
	 * @param model
	 */
	public void update(T model);
}